package co.g2academy.android_room;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import java.util.List;

public class NasabahViewModel extends AndroidViewModel {
    private NasabahRepository repository;
    private LiveData<List<Nasabah>> allNasabahs;

    public NasabahViewModel(@NonNull Application application) {
        super(application);
        repository = new NasabahRepository(application);
        allNasabahs = repository.getAllNasabahs();
    }

    public void insert(Nasabah nasabah) {
        repository.insert(nasabah);
    }

    public void update(Nasabah nasabah) {
        repository.update(nasabah);
    }

    public void delete(Nasabah nasabah) {
        repository.delete(nasabah);
    }

    public void deleteAllNasabahs() {
        repository.deleteAllNasabahs();
    }

    public LiveData<List<Nasabah>> getAllNasabahs() {
        return allNasabahs;
    }
}