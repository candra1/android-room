package co.g2academy.android_room;

import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import androidx.annotation.NonNull;

@Database(entities = {Nasabah.class}, version = 1)
public abstract class NasabahDatabase extends RoomDatabase {

    private static NasabahDatabase instance;

    public abstract NasabahDao nasabahDao();

    public static synchronized NasabahDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    NasabahDatabase.class, "nasabah_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private NasabahDao nasabahDao;

        private PopulateDbAsyncTask(NasabahDatabase db) {
            nasabahDao = db.nasabahDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            nasabahDao.insert(new Nasabah("Candra", "Jakarta Selatan", "candra@g2academy.co"));
            nasabahDao.insert(new Nasabah("Budi", "Jakarta Utara", "budi@gmail.com"));
            nasabahDao.insert(new Nasabah("Rini", "Jakarta Pusat", "rini@gmail.com"));
            return null;
        }
    }
}