package co.g2academy.android_room;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NasabahAdapter extends ListAdapter<Nasabah, NasabahAdapter.NasabahHolder> {

    private OnItemClickListener listener;

    public NasabahAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<Nasabah> DIFF_CALLBACK = new DiffUtil.ItemCallback<Nasabah>() {
        @Override
        public boolean areItemsTheSame(@NonNull Nasabah oldItem, @NonNull Nasabah newItem) {
            return oldItem.getD() == newItem.getD();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Nasabah oldItem, @NonNull Nasabah newItem) {
            return oldItem.getNama().equals(newItem.getNama()) &&
                    oldItem.getAlamat().equals(newItem.getAlamat()) &&
                    oldItem.getEmail() == newItem.getEmail();
        }
    };

    @NonNull
    @Override
    public NasabahHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_nasabah, parent, false);
        return new NasabahHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NasabahHolder holder, int position) {
        Nasabah currentNasabah = getItem(position);
        holder.textViewNama.setText(currentNasabah.getNama());
        holder.textViewAlamat.setText(currentNasabah.getAlamat());
        holder.textViewEmail.setText(currentNasabah.getEmail());
    }

    public Nasabah getNasabahAt(int position) {
        return getItem(position);
    }

    class NasabahHolder extends RecyclerView.ViewHolder {
        private TextView textViewNama;
        private TextView textViewAlamat;
        private TextView textViewEmail;

        public NasabahHolder(View itemView) {
            super(itemView);
            textViewNama = itemView.findViewById(R.id.namaTextView);
            textViewAlamat = itemView.findViewById(R.id.alamatTextView);
            textViewEmail = itemView.findViewById(R.id.emailTextView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(getItem(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Nasabah note);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
