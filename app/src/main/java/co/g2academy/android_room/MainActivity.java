package co.g2academy.android_room;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int ADD_NASABAH_REQUEST = 1;
    public static final int EDIT_NASABAH_REQUEST = 2;

    private NasabahViewModel nasabahViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton buttonAddNasabah = findViewById(R.id.button_add_nasabah);
        buttonAddNasabah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddEditNasabahActivity.class);
                startActivityForResult(intent, ADD_NASABAH_REQUEST);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final NasabahAdapter adapter = new NasabahAdapter();
        recyclerView.setAdapter(adapter);

        nasabahViewModel = ViewModelProviders.of(this).get(NasabahViewModel.class);
        nasabahViewModel.getAllNasabahs().observe(this, new Observer<List<Nasabah>>() {
            @Override
            public void onChanged(@Nullable List<Nasabah> nasabahs) {
                adapter.submitList(nasabahs);
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                nasabahViewModel.delete(adapter.getNasabahAt(viewHolder.getAdapterPosition()));
                Toast.makeText(MainActivity.this, "Nasabah deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);

        adapter.setOnItemClickListener(new NasabahAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Nasabah nasabah) {
                Intent intent = new Intent(MainActivity.this, AddEditNasabahActivity.class);
                intent.putExtra(AddEditNasabahActivity.EXTRA_ID, nasabah.getD());
                intent.putExtra(AddEditNasabahActivity.EXTRA_NAMA, nasabah.getNama());
                intent.putExtra(AddEditNasabahActivity.EXTRA_ALAMAT, nasabah.getAlamat());
                intent.putExtra(AddEditNasabahActivity.EXTRA_EMAIL, nasabah.getEmail());
                startActivityForResult(intent, EDIT_NASABAH_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_NASABAH_REQUEST && resultCode == RESULT_OK) {
            String nama = data.getStringExtra(AddEditNasabahActivity.EXTRA_NAMA);
            String alamat = data.getStringExtra(AddEditNasabahActivity.EXTRA_ALAMAT);
            String email = data.getStringExtra(AddEditNasabahActivity.EXTRA_EMAIL);

            Nasabah nasabah = new Nasabah(nama, alamat, email);
            nasabahViewModel.insert(nasabah);

            Toast.makeText(this, "Nasabah saved", Toast.LENGTH_SHORT).show();
        } else if(requestCode == EDIT_NASABAH_REQUEST && resultCode == RESULT_OK) {

            // edit metodundan dönen id'yi okuyalım, eğer id yoksa default -1 olsun
            int id = data.getIntExtra(AddEditNasabahActivity.EXTRA_ID, -1);

            if (id == -1) {
                Toast.makeText(this, "Nasabah can't be updated", Toast.LENGTH_SHORT).show();
                return;
            }

            String nama = data.getStringExtra(AddEditNasabahActivity.EXTRA_NAMA);
            String alamat = data.getStringExtra(AddEditNasabahActivity.EXTRA_ALAMAT);
            String email = data.getStringExtra(AddEditNasabahActivity.EXTRA_EMAIL);

            Nasabah nasabah = new Nasabah(nama, alamat, email);
            nasabah.setD(id);
            nasabahViewModel.update(nasabah);

            Toast.makeText(this, "Nasabah updated", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Nasabah not saved", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_all_nasabahs:
                nasabahViewModel.deleteAllNasabahs();
                Toast.makeText(this, "All nasabahs deleted", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}