package co.g2academy.android_room;

import android.app.Application;
import androidx.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class NasabahRepository {
    private NasabahDao nasabahDao;
    private LiveData<List<Nasabah>> allNasabahs;

    public NasabahRepository(Application application) {
        NasabahDatabase database = NasabahDatabase.getInstance(application);
        nasabahDao = database.nasabahDao();
        allNasabahs = nasabahDao.getAllNasabah();
    }

    public void insert(Nasabah nasabah) {
        new InsertNasabahAsyncTask(nasabahDao).execute(nasabah);
    }

    public void update(Nasabah nasabah) {
        new UpdateNasabahAsyncTask(nasabahDao).execute(nasabah);
    }

    public void delete(Nasabah nasabah) {
        new DeleteNasabahAsyncTask(nasabahDao).execute(nasabah);
    }

    public void deleteAllNasabahs() {
        new DeleteAllNasabahsAsyncTask(nasabahDao).execute();
    }

    public LiveData<List<Nasabah>> getAllNasabahs() {
        return allNasabahs;
    }

    private static class InsertNasabahAsyncTask extends AsyncTask<Nasabah, Void, Void> {
        private NasabahDao nasabahDao;

        private InsertNasabahAsyncTask(NasabahDao nasabahDao) {
            this.nasabahDao = nasabahDao;
        }

        @Override
        protected Void doInBackground(Nasabah... nasabahs) {
            nasabahDao.insert(nasabahs[0]);
            return null;
        }
    }

    private static class UpdateNasabahAsyncTask extends AsyncTask<Nasabah, Void, Void> {
        private NasabahDao nasabahDao;

        private UpdateNasabahAsyncTask(NasabahDao nasabahDao) {
            this.nasabahDao = nasabahDao;
        }

        @Override
        protected Void doInBackground(Nasabah... nasabahs) {
            nasabahDao.update(nasabahs[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("UpdateNasabahAsyncTask","Finish");
        }
    }

    private static class DeleteNasabahAsyncTask extends AsyncTask<Nasabah, Void, Void> {
        private NasabahDao nasabahDao;

        private DeleteNasabahAsyncTask(NasabahDao nasabahDao) {
            this.nasabahDao = nasabahDao;
        }

        @Override
        protected Void doInBackground(Nasabah... nasabahs) {
            nasabahDao.delete(nasabahs[0]);
            return null;
        }
    }

    private static class DeleteAllNasabahsAsyncTask extends AsyncTask<Void, Void, Void> {
        private NasabahDao nasabahDao;

        private DeleteAllNasabahsAsyncTask(NasabahDao nasabahDao) {
            this.nasabahDao = nasabahDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            nasabahDao.deleteAllNasabah();
            return null;
        }
    }
}