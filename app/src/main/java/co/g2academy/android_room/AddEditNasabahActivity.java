package co.g2academy.android_room;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

public class AddEditNasabahActivity extends AppCompatActivity {

    public static final String EXTRA_ID = "co.g2academy.android_room.EXTRA_ID";
    public static final String EXTRA_NAMA = "co.g2academy.android_room.EXTRA_NAMA";
    public static final String EXTRA_ALAMAT = "co.g2academy.android_room.EXTRA_ALAMAT";
    public static final String EXTRA_EMAIL = "co.g2academy.android_room.EXTRA_EMAIL";

    private EditText editTextNama;
    private EditText editTextAlamat;
    private EditText editTextEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_nasabah);

        editTextNama = findViewById(R.id.edit_text_nama);
        editTextAlamat = findViewById(R.id.edit_text_alamat);
        editTextEmail = findViewById(R.id.edit_text_email);


        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)) {
            setTitle("Edit Nasabah");
            editTextNama.setText(intent.getStringExtra(EXTRA_NAMA));
            editTextAlamat.setText(intent.getStringExtra(EXTRA_ALAMAT));
            editTextEmail.setText(intent.getStringExtra(EXTRA_EMAIL));
        } else {
            setTitle("Add Nasabah");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_nasabah_menu, menu);
        return true;
    }

    private void saveNasabah() {
        String nama = editTextNama.getText().toString();
        String alamat = editTextAlamat.getText().toString();
        String email = editTextEmail.getText().toString();

        if (nama.trim().isEmpty() || alamat.trim().isEmpty()) {
            Toast.makeText(this, "Please insert a nama and alamat", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_NAMA, nama);
        data.putExtra(EXTRA_ALAMAT, alamat);
        data.putExtra(EXTRA_EMAIL, email);

        // save metodu hem edit hem save işlemini yapacak, eğer id değeri ile geliyorsa edit yapsın
        // bir id değeri ile gelmiyor ise default -1 verelim ve o zamanda save yapsın
        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if (id != -1) {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_nasabah:
                saveNasabah();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
