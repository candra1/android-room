package co.g2academy.android_room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "nasabah_table")
public class Nasabah {

    @PrimaryKey(autoGenerate = true)
    private int d;

    private String nama;

    private String alamat;

    private String email;

    public Nasabah(String nama, String alamat, String email) {
        this.nama = nama;
        this.alamat = alamat;
        this.email = email;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}