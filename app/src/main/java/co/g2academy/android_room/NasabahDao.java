package co.g2academy.android_room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface NasabahDao {

    @Insert
    void insert(Nasabah nasabah);

    @Update
    void update(Nasabah nasabah);

    @Delete
    void delete(Nasabah nasabah);

    @Query("DELETE FROM nasabah_table")
    void deleteAllNasabah();

    @Query("SELECT * FROM nasabah_table ORDER BY nama DESC")
    LiveData<List<Nasabah>> getAllNasabah();
}
